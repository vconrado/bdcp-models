from .measurement import MeasurementProperties
from typing import List

class Collection(dict):
    def __init__(self,  id: str, 
                        name: str, 
                        description: str, 
                        license: str, 
                        extent: dict, 
                        dimensions: dict,
                        summaries: dict,
                        measurements: List[MeasurementProperties],
                        **kwargs):
        data = {
                "id": id,
                "name": name,
                "description": description,
                "license": license,
                "extent": extent,
                'dimensions': dimensions,
                'summaries':summaries,
                "measurements": measurements
            }

        for arg in kwargs:
            data[arg] = kwargs[arg]
        
        super(Collection, self).__init__(data)
    
    @property
    def id(self):
        return self['id']

    @property
    def name(self):
        return self['name']
    
    @property
    def description(self):
        return self['description']

    @property
    def license(self):
        return self['license']

    @property
    def extent(self):
        return self['extent']

    @property
    def dimensions(self):
        return self['dimensions']

    @property
    def summaries(self):
        return self['summaries']

    @property
    def measurements(self):
        return self['measurements']
        
    