from typing import List

class SceneProperties(dict):
    """The Scene properties object."""
    def __init__(self, source_id: str,
                       datetime: str, 
                       platform: str, 
                       instruments: List[str],
                       **kwargs):
        
        data = {
            'source_id': source_id,
            'datetime': datetime,
            'platform': platform,
            'instruments': instruments
        }
        for arg in kwargs:
            data[arg] = kwargs[arg]
        
        super(SceneProperties, self).__init__(data)
    
    @property
    def source_id(self) -> str:
        return self['source_id']

    @property
    def datetime(self) -> str:
        return self['datetime']
    
    @property
    def platform(self) -> str:
        return self['platform']
    
    @property
    def instruments(self) -> List[str]:
        return self['instruments']