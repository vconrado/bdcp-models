from enum import Enum

DATETIME_FORMAT="%Y-%m-%dT%H:%M:%S"

class DataType(Enum):

    # Source: https://github.com/opendatacube/datacube-core/blob/develop/datacube/model/schema/ingestor-config-type-schema.yaml
    
    float16     = "float16"
    float32     = "float32"
    float64     = "float64"
    int8        = "int8"
    int16       = "int16"
    int32       = "int32"
    int64       = "int64"
    uint8       = "uint8"
    uint16      = "uint16"
    uint32      = "uint32"
    uint64      = "uint64"
    complex64   = "complex64"
    complex128  = "complex128"
    sits_scene = "sits_scene"
