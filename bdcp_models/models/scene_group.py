from .measurement import Measurement
from .scene_properties import SceneProperties
from .local_scene import LocalScene
from .indexed_scene import IndexedScene

class SceneGroup(dict):
    def __init__(self, data: dict):
        """Initialize instance with dictionary data.

        :param data: Dict with IndexedSceneGroup metadata.
            expected format:
            {
                "key1":  SceneType,
                "key2":  SceneType, 
                ...
            }
        """
        super(SceneGroup, self).__init__(data or {})


    def merge(self, ref_group=None) -> LocalScene:
        # se nao tiver definido um grupo de referencia, usa o primeiro
        if ref_group is None:
            ref_group = list(self.keys())[0]
        
        # O Merge do lineage é feito a partir da concatenacao dos "classifiers" e da listagem dos source (se forem diferentes)

        classifiers = set()
        sources = set()
        properties = dict()
        for key in self.keys():
            for classifier in self[key].lineage:
                classifiers.add(classifier)
                for s in self[key].lineage[classifier]:
                    sources.add(s)
            if key != ref_group:
                properties.update(self[key].properties)
        
        # último a atualizar é o ref group
        properties.update(self[ref_group].properties)
        classifier = "_".join(sorted(classifiers))
        
        lineage = {
            classifier: list(sources)
        }

        merged = LocalScene(
            properties = SceneProperties(**properties),
            lineage = lineage
        )

        # for keys in self.keys():
        #     for measurement in self[keys].measurements:
        #         merged.add_measurement(measurement)
        return merged



class IndexedSceneGroup(SceneGroup):
    """The Indexed Scene Group object."""
    
    def __init__(self, data: dict):
        """Initialize instance with dictionary data.

        :param data: Dict with IndexedSceneGroup metadata.
            expected format:
            {
                "key1":  IndexedScene,
                "key2":  IndexedScene, 
                ...
            }
        """
        # Checking expected format:
        if not isinstance(data, dict):     
            raise AttributeError(f"Invalid type. Expecting dictionary")
        for k in data.keys():
            if not isinstance(data[k], IndexedScene):
                raise AttributeError(f"Invalid type. Expecting '{k}': IndexedScene")

        super(IndexedSceneGroup, self).__init__(data or {})


class LocalSceneGroup(SceneGroup):
    """The Local Scene Group object."""
    
    def __init__(self, data: dict):
        """Initialize instance with dictionary data.

        :param data: Dict with IndexedSceneGroup metadata.
            expected format:
            {
                "key1":  LocalScene,
                "key2":  LocalScene, 
                ...
            }
        """
        # Checking expected format:
        if not isinstance(data, dict):     
            raise AttributeError(f"Invalid type. Expecting dictionary")
        for k in data.keys():
            if not isinstance(data[k], LocalScene):
                raise AttributeError(f"Invalid type. Expecting '{k}': LocalScene")

        super(LocalSceneGroup, self).__init__(data or {})
