import os
from urllib.parse import urlparse
from pathlib import Path
import tarfile
import zipfile
import dateutil.parser as parser
import glob
from typing import Tuple

from .scene_properties import SceneProperties
from .local_cube import LocalScene
from .remote_scene import RemoteScene
from .data_type import DataType
from .measurement import Measurement, MeasurementProperties
from bdcp_utils import Utils
from bdcp_utils import get_logger


class StacRemoteScene(RemoteScene):

    PLATFORM_SENTINEL_2 = 'Sentinel-2'.upper()
    PLATFORM_LANDSAT_8  = 'Landsat-8'.upper()
    PLATFORM_SENTINEL_3 = 'SENTINEL-3'.upper()

    def __init__(self,  assets: dict, properties: SceneProperties):
        data = {
            'assets': assets,
            'properties': properties
        }
        super(StacRemoteScene, self).__init__(**data)
    
    @property
    def assets(self) -> dict:
        return self['assets']
    
    @property
    def properties(self) -> SceneProperties:
        return self['properties']
    
    @staticmethod
    def handle_sentinel_asset(filepath: Path, eo_bands: dict) ->Tuple[dict, dict]:

            def directory_find(atom, root):
                for path, dirs, files in os.walk(root):
                    directory = Path(os.path.join(path, atom))
                    if atom in dirs:
                        return directory
                    else:
                        dir_found = directory_find(atom, root=directory)
                        if dir_found:
                            return dir_found
                return None

            with zipfile.ZipFile(filepath, 'r') as zip_ref:
                zip_ref.extractall(filepath.parent)
                ms = {}
                img_data_dir = directory_find("IMG_DATA", filepath.parent)
                if not img_data_dir:
                    raise Exception("IMG_DATA folder not found inside Sentinel-2 SAFE.")
                for file in glob.glob(f"{img_data_dir}/*.jp2"):
                    band = Path(file).stem.split("_")[-1]
                    if band in eo_bands:
                        ms[band] = {
                            'path': file
                        }
            return ms, {}

    @staticmethod
    def handle_sentinel3_asset(filepath: Path, eo_bands: dict) ->Tuple[dict, dict]:
            logger = get_logger()
            logger.info(f"handle_sentinel3_asset {filepath} {eo_bands}")

            with zipfile.ZipFile(filepath, 'r') as zip_ref:
                zip_ref.extractall(filepath.parent)
                ms = {}
                for file in glob.glob(f"{filepath.parent}/Oa*_radiance.nc"):
                    band = Path(file).stem.split("_")[0].lower()
                    if band in eo_bands:
                        ms[band] = {
                            'path': file
                        }
            return ms, {}

    def download(self, dest_path: str, overwrite: bool=False) -> LocalScene:
        measurements = {}
        downloads = []

        for band in self.assets:
            href = self.assets[band].href
            parsed_href = urlparse(href)
            file_path = os.path.join(dest_path, os.path.basename(parsed_href.path))

            skip = False
            if os.path.exists(file_path) and not overwrite:
                if "bdc:size" in self.assets[band].extra_fields:
                    skip = os.path.getsize(file_path) == self.assets[band].extra_fields['bdc:size']

            if not skip:
                #Utils.download_file(url=href, file_path=file_path)
                downloads.append({
                    "url": href,
                    "file_path": file_path,
                    "size": self.assets[band].extra_fields['bdc:size'] if "bdc:size" in self.assets[band].extra_fields else None
                })

            measurements[band] = {'path': file_path}
        if len(downloads) ==  1:
            Utils.download_file(downloads[0]['url'], downloads[0]['file_path'])
        elif len(downloads) > 1:
            proc = Utils.download_files_parallel(downloads, 10)
            if (proc.returncode != 0):
                raise Exception("Downloading error", proc.stderr)

        eo_bands = {
            band['name']: band for band in self.properties.get('eo:bands', {}) if 'name' in band
        }        

        logger = get_logger()
        logger.info(f"properties {self.properties}")

        for band in list(measurements.keys()):
            filepath = Path(measurements[band]['path'])

            # handle tar file (landsat-8)
            if tarfile.is_tarfile(filepath) and self.properties.platform.upper() == StacRemoteScene.PLATFORM_LANDSAT_8:
                tar = tarfile.open(filepath, "r|gz")
                for t in tar:
                    bandfilename = Path(filepath.parent) / t.name
                    if bandfilename.suffix.lower() in [".tif", ".txt"]:
                        # descompacta se o arquivo nao existir ou se o tamanho do arquivo em disco for diferente do arquivo compactado
                        if not os.path.exists(bandfilename) or os.path.getsize(bandfilename) != t.size:
                            tar.extract(t, path=bandfilename.parent)
                        else:
                            print(f"Skipping extration of file {bandfilename}")
                        key = bandfilename.stem.split("_")[-1]
                        if bandfilename.suffix.lower() == ".tif":
                            measurements[key] = {
                                'path': str(bandfilename)
                            }
                        elif str(bandfilename).lower().endswith("mtl.txt"):
                            # obtendo o horário da imagem (para ser usado no 6S)
                            mtl = Utils.mtl_to_json(bandfilename)
                            center_time = mtl.get('L1_METADATA_FILE',{}).get('PRODUCT_METADATA',{}).get('SCENE_CENTER_TIME', '00:00:00')
                            date = parser.parse(self['properties']['datetime'])
                            date = parser.parse(f"{date.strftime('%Y-%m-%dT')}{center_time}")
                            self['properties']['datetime'] = date.strftime('%Y-%m-%dT%H:%M:%S')
                    else:
                        print(f"Skipping file {bandfilename} (not tif or txt)")
                tar.close()

                del measurements[band]
            
            # handle zip file (sentinel-2)
            elif zipfile.is_zipfile(filepath) and self.properties.platform.upper() == StacRemoteScene.PLATFORM_SENTINEL_2:
                ms, props = StacRemoteScene.handle_sentinel_asset(filepath, eo_bands)
                if ms:
                    measurements.update(ms)
                    del measurements[band]
                self.update(props)

            # handle zip file (sentinel-3)
            elif zipfile.is_zipfile(filepath) and self.properties.platform.upper() == StacRemoteScene.PLATFORM_SENTINEL_3:
                logger.info(f"PLATFORM_SENTINEL_3")
                ms, props = StacRemoteScene.handle_sentinel3_asset(filepath, eo_bands)
                logger.info(f"MS: {ms}")
                if ms:
                    measurements.update(ms)
                    del measurements[band]
                self.update(props)
                logger.info(f"self: {self}")


        # Create LocalScene
        local_scene = LocalScene(
            properties=SceneProperties(**{key: self['properties'][key] for key in self['properties'] if key not in ['eo:bands']})
        )

        

        for key in measurements:
            eo_band = eo_bands.get(key, {})
            local_scene.add_measurement(
                Measurement(
                    path=measurements[key]['path'], 
                    properties=MeasurementProperties(
                        name=eo_band.get("name", key), 
                        data_type=DataType.uint32,
                        max=eo_band.get("max", None), 
                        min=eo_band.get("min", None), 
                        scale=eo_band.get("scale", None), 
                        nodata=eo_band.get("nodata", None), 
                        common_name=eo_band.get("common_name", None), 
                        description=eo_band.get("description", ''), 
                        center_wavelength=eo_band.get("center_wavelength", None), 
                        full_width_half_max=eo_band.get("full_width_half_max", None), 
                    )
                )
            )
            
        logger.info(f"local_scene: {local_scene}")

        return local_scene
