from .scene_properties import SceneProperties

class Scene(dict):
    """The Scene object."""
    def __init__(self,  properties: SceneProperties, **kwargs):

        if not isinstance(properties, SceneProperties):
            raise Exception("properties must be SceneProperties")

        kwargs['properties'] = properties
        super(Scene, self).__init__(kwargs)

    @property
    def properties(self) -> SceneProperties:
        return self['properties']