from .scene_properties import SceneProperties
from .local_cube import LocalScene
from .scene import Scene

class RemoteScene(Scene):
    """The Remote Scene object."""
    def __init__(self,  properties: SceneProperties, **kwargs):

        if not isinstance(properties, SceneProperties):
            raise Exception("properties must be SceneProperties")

        super(RemoteScene, self).__init__(properties=properties, **kwargs)

    @property
    def properties(self) -> SceneProperties:
        return self['properties']
    
    def download(self, dest_path: str, overwrite: bool=False) -> LocalScene:
        raise NotImplemented("You must implement download method")