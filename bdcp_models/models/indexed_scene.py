from .measurement import Measurement
from .local_cube import LocalScene
from .scene_properties import SceneProperties
from typing import List

class IndexedScene(LocalScene):
    """The Indexed Scene object."""
    def __init__(self,  id: str, 
                        crs: str, 
                        grids: dict, 
                        extent: dict, 
                        lineage: dict,
                        product: dict,
                        geometry: dict,
                        properties: SceneProperties, 
                        grid_spatial: dict, 
                        measurements: List[Measurement],
                        **kwargs):
        

        if not isinstance(properties, SceneProperties):
            raise Exception("properties must be SceneProperties")

        super(IndexedScene, self).__init__( lineage=lineage, 
                                            properties=properties,
                                            measurements=measurements)
        self['id'] = id
        self['crs']= crs 
        self['grids']= grids 
        self['extent']= extent
        self['product']= product
        self['geometry']= geometry
        self['grid_spatial']= grid_spatial
        
        for arg in kwargs:
            self[arg] = kwargs[arg]

    @property
    def id(self)-> str:
        return self['id']

    @property
    def crs(self) -> str:
        return self['crs']

    @property
    def grids(self)-> dict:
        return self['grids']
    
    @property
    def extent(self)-> dict:
        return self['extent']
    
    @property
    def lineage(self)-> dict:
        return self['lineage']

    @property
    def schema(self)-> str:
        return self['$schema']
    
    @property
    def product(self)-> dict:
        return self['product']

    @property
    def geometry(self)-> dict:
        return self['geometry']

    @property
    def properties(self)-> SceneProperties:
        return self['properties']

    @property
    def grid_spatial(self)-> dict:
        return self['grid_spatial']

    @property
    def measurements(self)-> List[Measurement]:
        return self['measurements']
