from pathlib import Path
from .scene_properties import SceneProperties
from .local_cube import LocalScene
from .remote_scene import RemoteScene
from .data_type import DataType
from .measurement import Measurement, MeasurementProperties
import shutil


class SitsRemoteScene(RemoteScene):
    """The Remote Scene object."""

    def __init__(self, properties: SceneProperties, path: str, **kwargs):

        if not isinstance(properties, SceneProperties):
            raise Exception("properties must be SceneProperties")
        super(RemoteScene, self).__init__(properties=properties, path=path, **kwargs)

    @property
    def path(self) -> str:
        return self["path"]

    @property
    def properties(self) -> SceneProperties:
        return self["properties"]

    def download(self, dest_path: str, overwrite: bool = False) -> LocalScene:
        source = Path(self.path)
        dest = Path(dest_path, source.name)
        if not dest.exists() or overwrite:
            shutil.move(str(source), str(dest))

        return LocalScene(
            properties=self.properties,
            measurements=Measurement(
                path=dest,
                properties=MeasurementProperties(
                    name="sits", data_type=DataType.sits_scene
                ),
            ),
        )
