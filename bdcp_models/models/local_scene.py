import uuid
from typing import OrderedDict
import rasterio as rio
from typing import List
from .measurement import Measurement
from .scene_properties import SceneProperties
from .scene import Scene
from .data_type import DATETIME_FORMAT


class LocalScene(Scene):
    """The Local Scene object."""
    
    def __init__(self, properties: SceneProperties, 
                       lineage: dict=None, 
                       measurements: List[Measurement] = None):

        if not isinstance(properties, SceneProperties):
            raise Exception("properties must be SceneProperties")

        kargs = {
            'lineage': lineage if lineage else {},
            'measurements': measurements if measurements else list()
        }

        super(LocalScene, self).__init__(properties=properties, **kargs)
    
    def remove(self):
        for measurement in self.measurements:
            measurement.remove()
        self.measurements = list()

    @property
    def lineage(self)-> dict:
        return self['lineage']
    
    def add_lineage(self, lineage: dict):
        self['lineage'].update(lineage)

    @property
    def properties(self) -> SceneProperties:
        return self['properties']

    def add_properties(self, props: dict):
        self['properties'].update(props)

    @property
    def measurements(self)-> List[Measurement]:
        return self['measurements']   

    def measurement(self, name)-> Measurement:
        for m in self.measurements:
            if m.properties.name == name:
                return m
        return None

    def add_measurement(self, measurement: Measurement):
        self['measurements'].append(measurement)

    @property
    def bounds(self) -> rio.coords.BoundingBox:
        if len(self.measurements) == 0:
            return None
        datasource = rio.open(self.measurements[0].path)
        return datasource.bounds
        

    def to_odc_item(self, dc_product: dict) -> dict:
        
        dc_item = OrderedDict()
        dc_item["$schema"] = "https://schemas.opendatacube.org/dataset"
        
        # id gerado do hash da concatenacao de todos os paths de todos os atributos
        paths = ",".join([f"[{measurement.properties.name}]={measurement.path}" for measurement in self.measurements])

        dc_item['id'] = str(uuid.uuid5(uuid.NAMESPACE_URL, paths))

        # product
        dc_item["product"] = OrderedDict({
            "name": dc_product['name'],
        })
        
        dc_item["properties"] = dict(self.properties)
        dc_item["properties"]["eo:bands"] = [dict(m.properties) for m in self.measurements]

        if not self.properties['instruments']:
            self.properties['instruments'] = dc_product['metadata']['instruments']

        dc_item["measurements"] = OrderedDict()
                 
        for measurement in self.measurements:
            dc_item["measurements"][measurement.properties.name] = {
                "path": measurement.path
            }
            # just for the first item, collect some geo metatada
            if "grids" not in dc_item:
                datasource = rio.open(measurement.path)

                crs = datasource.read_crs().to_string()

                if not crs.startswith("EPSG"):
                    crs = dc_product["storage"]["crs"]

                dc_item["crs"] = crs

                dc_item["grids"] = OrderedDict({
                    "default": {
                        "shape": list(datasource.shape),
                        "transform": list(datasource.transform)
                    }
                })
                bounds = self.bounds
                
                coordinates =  [[
                    [bounds.left, bounds.top],     # left, top
                    [bounds.right, bounds.top],    # right, top
                    [bounds.right, bounds.bottom], # right, bottom
                    [bounds.left, bounds.bottom],  # left, bottom
                    [bounds.left, bounds.top]      # left, top
                ]]           

                dc_item["geometry"] = OrderedDict({
                    "type": "Polygon",
                    "coordinates":  coordinates  
                })
               
 
        dc_item["lineage"] = dict(self.lineage)
        
        return dc_item
    
    def merge(self, other):
        if not isinstance(other, LocalScene):
            raise Exception(f"Expected LocalScene object, received {type(other)}.")
        
        # measurement
        for measurement in other.measurements:
            self.add_measurement(measurement)
        
        # properties
        properties = other.properties.copy()
        properties.update(self.properties)
        self['properties'] = SceneProperties(**properties)


        # lineage
        # O Merge do lineage é feito a partir da concatenacao dos "classifiers" 
        # e da listagem dos source (se forem diferentes)

        # all classifiers
        classifiers = {classifier for classifier in self.lineage}
        classifiers.update({classifier for classifier in other.lineage})

        # all sources
        sources = {s for classifier in other.lineage for s in other.lineage[classifier]}
        sources.update({s for classifier in other.lineage for s in other.lineage[classifier]})
        
        classifier = "_".join(sorted(classifiers))
        
        self['lineage'] = {
            classifier: list(sources)
        }





        


        

