import datetime
from typing import List, Union
from .local_scene import LocalScene
from .data_type import DATETIME_FORMAT
import rasterio as rio
from pathlib import Path
import os 
import subprocess

class LocalCube(dict):
    def __init__(self, scenes: List[LocalScene] = []):
        
        data = {
            'scenes': [],
            'bounds': None
        }

        super(LocalCube, self).__init__(data)

        for scene in scenes:
            self.add(scene)

    def save_mosaic(self, dest_file: Union[str, Path]):
        
        # TODO implementar completo
        # merge same data
        # crop bbox
        # cria vrt com as bandas
        # Se tiver +1 tempo apenda o dt no nome da banda
        # Para cada dt, salva a banda BX_DT

        if len(self.timeseries) > 1:
            raise Exception("Multiple timestamps mosaic not implemented.")

        path = Path(dest_file)
        parent_dir = path.parent
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir, exist_ok=True)

        files = []
        for scene in self.scenes:
            for m in scene.measurements:
                files.append(m.path)

        files = " ".join(files)

        # command = f'gdalwarp -te {self.bounds["left"]} {self.bounds["bottom"]} {self.bounds["right"]} {self.bounds["top"]} {scene.} {band_path}'
        command = f'gdal_merge.py -separate  -o {dest_file} {files}'

        proc = subprocess.run(command, shell = True, capture_output=True)
        if proc.returncode:
            logger = get_logger()
            logger.error("Error with gdal_merge")
            logger.error(proc.stdout)
            logger.error(proc.stderr)
            raise Exception(f"Error with gdal_merge. Code: '{proc.returncode}'")  
    
    @staticmethod
    def merge_bbox(bbox1: dict, bbox2: dict) -> dict:
        bbox = {
            'left': min(bbox1['left'] , bbox2['left']),
            'bottom': min(bbox1['bottom'], bbox2['bottom']),
            'right': max(bbox1['right'], bbox2['right']),
            'top': max(bbox1['top'], bbox2['top'])
        }
        return bbox   

    def add(self, scene: LocalScene):

        def to_dict(bounds: rio.coords.BoundingBox) -> dict:
            return {"left": bounds.left, "bottom": bounds.bottom, "right": bounds.right, "top": bounds.top}

        self["scenes"].append(scene)
        self["scenes"].sort(key=lambda s: s.properties.datetime)
        if self["bounds"]:
            self["bounds"] = LocalCube.merge_bbox(to_dict(scene.bounds), self['bounds'])
        else:
            bounds = scene.bounds
            if bounds:
                self["bounds"] = to_dict(bounds)

    @property
    def scenes(self) -> List[LocalScene]:
        return self["scenes"]

    @property
    def timeseries(self) -> List[datetime.datetime]:
        ts = list(set(datetime.datetime.strptime(t.properties.datetime, DATETIME_FORMAT) for t in self["scenes"]))
        ts.sort()
        return ts


    def by_date(self, dt : datetime.datetime) -> List[LocalScene]:
        return [scene for scene in self.scenes if dt.date() == datetime.datetime.strptime(scene.properties.datetime, DATETIME_FORMAT).date()]

    @property
    def bounds(self) -> rio.coords.BoundingBox:
        bbox = self["bounds"]
        return rio.coords.BoundingBox(
                left=bbox["left"],
                bottom=bbox["bottom"],
                right=bbox["right"],
                top=bbox["top"]
            )



