from .scene_properties import SceneProperties
from .local_scene import LocalScene
from .remote_scene import RemoteScene
from bdcp_utils import Utils

import glob
import pathlib

class UranusRemoteScene(RemoteScene):
    def __init__(self,  base_url: str, properties: SceneProperties):
        data = {
            'base_url': base_url,
            'properties': properties
        }
        super(UranusRemoteScene, self).__init__(**data)
    
    def download(self, dest_path: str, overwrite: bool=False) -> LocalScene:
        
        # volta um nivel das pastas, pq o gcsutil já baixa na pasta de source_id
        path = pathlib.Path(dest_path)
        download_path = Utils.gcs_download(self['base_url'], str(path.parent.absolute()), overwrite)

        # Create LocalScene
        local_scene = LocalScene(
            lineage= {},
            measurements={},
            properties=self['properties']
        )
        files = glob.glob(f"{download_path}/*.TIF")
        for file in files:
            fp = pathlib.Path(file)
            m_name = fp.stem.split("_")[-1]
            local_scene.add_measurement(m_name, file)

        if len(local_scene.measurements) < 1:
            raise Exception("Error downloading UranusScene")
        
        return local_scene