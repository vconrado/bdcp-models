from .measurement import Measurement
from .local_cube import LocalCube
from .scene_properties import SceneProperties
from typing import List

class IndexedCube(LocalCube):
    pass