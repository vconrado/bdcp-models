from typing import Union
from pathlib import Path
from .data_type import DataType
import os

class MeasurementProperties(dict):
    def __init__(self,  name: str, 
                        data_type: DataType,
                        max: float=None, 
                        min: float=None,
                        scale: float=None,
                        nodata: Union[float, str]=None,
                        common_name: str="",
                        description: str="",
                        center_wavelength: float=None,
                        full_width_half_max: float=None,
                        **kwargs):

        data = {
            "name": name,
            "max": max,
            "min": min,
            "scale": scale,
            "nodata": nodata,
            "data_type": data_type.name,
            "common_name": common_name,
            "description": description,
            "center_wavelength": center_wavelength,
            "full_width_half_max": full_width_half_max
        }

        for arg in kwargs:
            data[arg] = kwargs[arg]
        
        super(MeasurementProperties, self).__init__(data)
    
    @property
    def name(self) -> str:
        return self['name']

    @property
    def max(self) -> float:
        return self['max']
    
    @property
    def min(self) -> float:
        return self['min']
    
    @property
    def scale(self) -> float:
        return self['scale']

    @property
    def nodata(self)-> Union[float, str]:
        return self['nodata']
    
    @property
    def data_type(self) -> DataType:
        return self['data_type']

    @property
    def common_name(self) -> str:
        return self['common_name']

    @property
    def description(self) -> str:
        return self['description']
    
    @property
    def center_wavelength(self) -> float:
        return self['center_wavelength']
    
    @property
    def full_width_half_max(self) -> float:
        return self['full_width_half_max']


class Measurement(dict):
    def __init__(self, path: Union[Path, str], properties: MeasurementProperties):

        if not isinstance(properties, MeasurementProperties):
            raise Exception("properties must be MeasurementProperties")

        data = {
            'path': str(path),
            'properties': properties,
        }

        super(Measurement, self).__init__(data)

    def remove(self):
        pfile = Path(self.path)
        if pfile.is_file():
            pfile.unlink(missing_ok=True)

    @property
    def path(self) -> str:
        return self['path']

    @property
    def properties(self) -> MeasurementProperties:
        return self['properties']

    
